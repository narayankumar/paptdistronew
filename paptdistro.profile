<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function paptdistro_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  // Admin account.
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  // Server settings.
  $form['server_settings']['site_default_country']['#default_value'] = 'IN';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Asia/Kolkata';
  // Update notifications.
  $form['update_notifications']['update_status_module']['#default_value'] = array();
  // Disable validate.
  $form['#validate'] = array();
}
/**
 * Implements hook_form_alter().
 */
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  // select paptdistro install profile by default
  foreach ($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = 'paptdistro';
  }
}